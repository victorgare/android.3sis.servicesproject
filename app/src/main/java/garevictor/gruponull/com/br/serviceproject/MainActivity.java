package garevictor.gruponull.com.br.serviceproject;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText edtSegundos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void disparar(View v){

        //OBTENDO OS SEGUNDOS E TRANSFORMANDO EM INTEIRO
        edtSegundos = (EditText) findViewById(R.id.edtSegundos);
        int i = Integer.parseInt(edtSegundos.getText().toString());


        //PREPARANDO COMUNICAÇÃO COM O BROADCAST
        Intent intent = new Intent(this, AlarmReceiver.class);
        PendingIntent pending = PendingIntent.getBroadcast(this.getApplicationContext(), 0, intent, 0);

        //ACIONA O SERVIÇO DE ALARME DO SISTEMA
        AlarmManager aManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        aManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + (i*1000), pending);

        Toast.makeText(this, "Alarm set to: " + i, Toast.LENGTH_SHORT).show();

    }

    public void validar(View v){
        //inicia  INTENT DO SERVIÇO DE NOTIFICAÇÃO
        Intent i = new Intent(this, ValidaService.class);

        //OBTENDO OS SEGUNDOS E TRANSFORMANDO EM INTEIRO
        edtSegundos = (EditText) findViewById(R.id.edtSegundos);
        int segundos = Integer.parseInt(edtSegundos.getText().toString());

        //ADICIONA OS SEGUNDOS PARA A ACTIVITY DE NOTIFICAÇÃO
        i.putExtra("segundos", segundos);

        //INICIA A ACTIVITY
        startService(i);
    }
}
