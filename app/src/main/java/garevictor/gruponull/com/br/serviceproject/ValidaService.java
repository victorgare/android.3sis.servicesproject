package garevictor.gruponull.com.br.serviceproject;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.v7.app.NotificationCompat;

public class ValidaService extends Service {
    public ValidaService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    public int onStartCommand(Intent intent, int flag, int startId){

        int segundos = intent.getExtras().getInt("segundos");

        if(segundos > 10){

            //CONTROLE DE NOTIFICAÇÃO
            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

            //CORPO DA NOTIFICAÇÃO
            NotificationCompat.Builder nBuilder = new NotificationCompat.Builder(this);
            nBuilder.setContentTitle("Validação");
            nBuilder.setContentText("Mensagem de validação de alarm");
            nBuilder.setTicker("Nova Mensagem de Alarme");
            nBuilder.setSmallIcon(R.drawable.fiap);

            //ACIONA NOTIFICAÇÃO
            notificationManager.notify(100, nBuilder.build());

            //CHAMANDO A ACTIVITY PRINCIPAL
            nBuilder.setContentIntent(PendingIntent.getActivity(this, 0, new Intent(this, MainActivity.class), PendingIntent.FLAG_UPDATE_CURRENT));
        }

        return START_STICKY;
    }
}
